import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddTodoComponent } from './add-todo/add-todo.component';
import { TodoListComponent } from './todo-list/todo-list.component'

const routes: Routes = [
  { path: 'addTask', component: AddTodoComponent },
  { path: 'todoList', component: TodoListComponent },
  { path: 'updateTask/:id', component: AddTodoComponent },
  { path: '', redirectTo: '/todoList', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
