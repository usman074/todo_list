import { Component, OnInit } from '@angular/core';
import { TaskListService } from '../task-list.service';
import { taskStructure } from '../taskStructure';
@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  constructor(private taskList : TaskListService) { }

  tasks : taskStructure[] = [];
  tasksCompleted : taskStructure[] =[];
  ngOnInit() {
    this.tasks = this.taskList.getTaskList();
  }

  delTask(id : number) : void{
    this.tasks = this.taskList.delTask(id);
  }
  onCompleted(task : taskStructure) : void{
    this.tasksCompleted.push(task);
    this.tasks = this.taskList.updateTask(task.id);
  }
  
  // addTask(taskName : string) :void {
  //   this.taskList.addTask(taskName);
  //   this.tasks = this.taskList.getTaskList();
  // }
}
